### Seminars in IIT Kharagpur..
Seminars Conducted in 2016/17
1.Voxeljet 3D printing Technology
2.Dynamic Instrumentation using PIN Tool

Brief :
1.Seminar on 3D printing -
Submitted in Oct-2016 As MTech First Year Seminar presentation.
Submitted to Prof Partha Bhomik - IIT Kharagpur - CSE Dept.

Technical Details -
 3D printing is also known as Additive Manufacturing.
 It is used to create Solid Object from Digital File.
 Additive process involves creation of objects by laying down successive layers of material 
 until the object is created. 
 Each of these layers can be seen as a thinly sliced horizontal cross-section of the eventual 
 object.
 The Methods used in Voxel Jet printing are discussed in this seminar like
 a. Material Jetting and
 b. Voxel jet 3D printing Technology
 
2.PIN Tool for Dynamic Instrumentation
   PIN tool is used to analyze the code on the fly, and retrieve the information about the code like about the methods,
   instructions, routines etc.
   The seminar discusses about - the source instrumentation and binary instrumentation, available tools, their usage 
   and architecture details of PIN tool.
 